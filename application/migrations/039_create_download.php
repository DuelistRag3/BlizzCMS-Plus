<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_create_download extends CI_Migration {

    public function up()
    {
      $this->dbforge->add_field(array(
              'id' => array(
                      'type' => 'INT',
                      'constraint' => '11',
                      'unsigned' => TRUE,
                      'auto_increment' => TRUE
              ),
              'file_name' => array(
                      'type' => 'VARCHAR',
                      'constraint' => '30'
              ),
              'title' => array(
                      'type' => 'varcher',
                      'constraint' => '64'
              ),
              'type' => array(
                        'type' => 'varcher',
                        'constraint' => '24'
              ),
              'weight' => array(
                        'type' => 'varcher',
                        'constraint' => '24'
              ),
              'description' => array(
                      'type' => 'TEXT'
              ),
              'url' => array(
                        'type' => 'varcher',
                        'constraint' => '255'
              ),
              'image' => array(
                        'type' => 'varcher',
                        'constraint' => '255'
              ),
      ));
      $this->dbforge->add_key('id', TRUE);
      $this->dbforge->create_table('download');
    }

    public function down()
    {
      $this->dbforge->drop_table('download');
    }
}
